//
//  SignUpVC.h
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "txtFieldCell.h"
#import <CoreLocation/CoreLocation.h>

@interface SignUpVC : UIViewController<UITextFieldDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>

@end
