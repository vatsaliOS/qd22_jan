//
//  DetailBistroVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "DetailBistroVC.h"
#import "DetailBistroCell.h"
#import "WaitingTimeVC.h"
#import <MapKit/MapKit.h>

@interface DetailBistroVC ()
{
    IBOutlet UIButton *joinQueueBtn;
    __weak IBOutlet UILabel *titleLbl;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet UILabel *avgWaitingTime;
    __weak IBOutlet UILabel *addressLbl;
    __weak IBOutlet UILabel *restaurantNameLbl;
    __weak IBOutlet UIImageView *restaurantImgView;
    __weak IBOutlet UITextView *addressTxtView;
    __weak IBOutlet UIButton *slowTimeAlertBtn;
}
@end

@implementation DetailBistroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [joinQueueBtn.layer setCornerRadius:5.0f];
    [self loadUI];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

- (void)viewDidLayoutSubviews {
    [addressTxtView setContentOffset:CGPointZero animated:NO];
}

#pragma mark - LoadUI
-(void)loadUI
{
    titleLbl.text = [[self.restaurantDetailDict valueForKey:@"name"] uppercaseString];
    avgWaitingTime.text = [NSString stringWithFormat:@"%@ min average wait",[self.restaurantDetailDict valueForKey:@"average_waiting_time"]];
    [restaurantImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.restaurantDetailDict valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];

    MKCoordinateRegion region = mapView.region;
    
    double latitude = [[self.restaurantDetailDict valueForKey:@"latitude"] doubleValue];
    double longitude = [[self.restaurantDetailDict valueForKey:@"longitude"] doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    CLLocation *location = [locationManager location];
    coordinate.longitude = location.coordinate.longitude;
    coordinate.latitude = location.coordinate.latitude;
    
    region.center = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    region.span.longitudeDelta /= 500.0; // Bigger the value, closer the map view
    region.span.latitudeDelta /= 500.0;
    [mapView setRegion:region animated:YES]; // Choose if you want animate or not
    
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];

    mapPin.title = titleLbl.text;
    mapPin.coordinate = coordinate;
    
    [mapView addAnnotation:mapPin];

    restaurantNameLbl.text = titleLbl.text;
    addressLbl.text = [self.restaurantDetailDict valueForKey:@"address"];
    
    addressTxtView.text = [self.restaurantDetailDict valueForKey:@"address"];
    
    if ([[self.restaurantDetailDict valueForKey:@"slowmode"] isEqualToNumber:[NSNumber numberWithInt:0]]) {
        [slowTimeAlertBtn setImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
    }
    else
    {
        [slowTimeAlertBtn setImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
    }
}

#pragma mark - MapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *reuseId = @"currentloc";
    
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (annView == nil)
    {
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        annView.animatesDrop = NO;
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-5, 5);
    }
    else
    {
        annView.annotation = annotation;
    }
    
    annView.pinColor = MKPinAnnotationColorPurple;
    return annView;
}


#pragma mark - TableView Methods

- (UIView*) tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(100, 200, tableView.bounds.size.width, 200)];
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(12,15,200,25)];
    [customLabel setFont:[UIFont systemFontOfSize:22]];
    [customLabel setTextColor:[UIColor darkGrayColor]];
    customLabel.text = @"Trading hours";
    [headerView setBackgroundColor:[UIColor whiteColor]];
    [headerView addSubview:customLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.restaurantDetailDict valueForKey:@"opening_hours"] count];
}                                                        

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailBistroCell *cell = (DetailBistroCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.backgroundColor = [UIColor whiteColor];
    //    cell.cellImgView.image = [UIImage imageNamed:@""];
    
    NSString *fontname = @"";
    if (indexPath.row == 0) {
        fontname = @"Hind-Bold";
        
        cell.dayLbl.text = @"Day";
        cell.startTimeLbl.text = @"Start Time";
        cell.endTimeLbl.text = @"End Time";
        
//        cell.dayLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
//        cell.startTimeLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
//        cell.endTimeLbl.font = [UIFont fontWithName:@"Hind-Bold" size:11.0f];
    }
    else
    {
        fontname = @"Hind-Regular";

        cell.dayLbl.text = [[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row]valueForKey:@"day"];
        cell.startTimeLbl.text = [[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row]valueForKey:@"start"];
        cell.endTimeLbl.text = [[[self.restaurantDetailDict valueForKey:@"opening_hours"]objectAtIndex:indexPath.row]valueForKey:@"end"];
        
//        cell.dayLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
//        cell.startTimeLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
//        cell.endTimeLbl.font = [UIFont fontWithName:@"Hind-Regular" size:11.0f];
    }
    
    if (IS_IPHONE_4_OR_LESS) {
        cell.dayLbl.font = [UIFont fontWithName:fontname size:11.0f];
        cell.startTimeLbl.font = [UIFont fontWithName:fontname size:11.0f];
        cell.endTimeLbl.font = [UIFont fontWithName:fontname size:11.0f];
    }
    else if (IS_IPHONE_5){
        cell.dayLbl.font = [UIFont fontWithName:fontname size:12.0f];
        cell.startTimeLbl.font = [UIFont fontWithName:fontname size:12.0f];
        cell.endTimeLbl.font = [UIFont fontWithName:fontname size:12.0f];
    }
    else
    {
        cell.dayLbl.font = [UIFont fontWithName:fontname size:15.0f];
        cell.startTimeLbl.font = [UIFont fontWithName:fontname size:15.0f];
        cell.endTimeLbl.font = [UIFont fontWithName:fontname size:15.0f];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark BackBtn Click
-(IBAction)backBtnClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma JoinQueue Btn Click
- (IBAction)joinQueueBtnClk:(id)sender
{
    [self performSegueWithIdentifier:@"WaitingTimeVC" sender:nil];
}

#pragma mark - SlowTimeAlert Click
-(IBAction)slowAlertBtnClk:(id)sender
{
    if ([slowTimeAlertBtn.imageView.image isEqual:[UIImage imageNamed:@"OffBtn"]]) {
        [slowTimeAlertBtn setImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
    }
    else
    {
        [slowTimeAlertBtn setImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    WaitingTimeVC *VC = (WaitingTimeVC*)[segue destinationViewController];
    
}


@end
