//
//  SettingsVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsCell.h"
#import "SettingsCell.h"

@interface SettingsVC ()
{
    BOOL isFiltered;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 10.0f)];

}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

#pragma mark - Back Clicked
- (IBAction)backBtnClk:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowFadeImg" object:nil];
}

#pragma mark TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell *cell = (SettingsCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"demo_img_%d",indexPath.row+1]];
    cell.titleLbl.text = @"Bagatelli's";
    cell.addressLbl.text = @"471 Storm Rd";
    cell.onOffBtn.tag = indexPath.row;
    [cell.onOffBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(IBAction)btnClick:(UIButton*)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    SettingsCell *cell = [self.tblView cellForRowAtIndexPath:indexPath];
    
    UIImage *btnBgImg = cell.onOffBtn.currentBackgroundImage;
    
    UIImage *compareWithImg = [UIImage imageNamed:@"OnBtn"];
    
    if ([btnBgImg isEqual:compareWithImg]) {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.onOffBtn setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
    }
    
    
//    if (cell.onOffBtn.isSelected) {
//        [sender setBackgroundImage:[UIImage imageNamed:@"OnBtn"] forState:UIControlStateNormal];
//        [sender setSelected: NO];
//    }
//    else
//    {
//        [sender setBackgroundImage:[UIImage imageNamed:@"OffBtn"] forState:UIControlStateNormal];
//        [sender setSelected:YES];
//    }
}

#pragma mark - SearchBar methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
        searchBar.showsCancelButton = NO;

    }
    else
    {
        //        [filteredTableData removeAllObjects];
        
        isFiltered = true;
        searchBar.showsCancelButton = YES;

        
        //        filteredTableData = [[NSMutableArray alloc] init];
        //
        //        int i=0;
        //        for (NSString *str in [poolTypesArray valueForKey:@"pooltype"])
        //        {
        //            NSRange nameRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
        //            NSRange descriptionRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
        //            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
        //            {
        //
        //                [filteredTableData addObject:[poolTypesArray objectAtIndex:i]];
        //            }
        //            i++;
        //        }
    }
    
    //    NSLog(@"filteredTableData == %@",filteredTableData);
    
    //    [tblView reloadData];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
//    self.viewTopConstraint.constant = 0.0f;
    [self.searchBar resignFirstResponder];
    self.searchBar.showsCancelButton = NO;
    self.searchBar.text = @"";
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
